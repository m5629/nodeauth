import path from "path";
import {createLogger, format, transports} from 'winston';
import 'winston-daily-rotate-file';

import {logsDirectory} from "../config";

let filename = 'combined';
let directory = logsDirectory;
let directoryName = 'logs';

if (!directory) directory = path.resolve(directoryName);

const dateFormat = 'MMM-DD-YYYY HH:mm:ss';

const formatOptions = format.combine(
  format.align(),
  format.timestamp({format: dateFormat}),
  format.printf(info => `${info.level} : ${[info.timestamp]} : ${info.message}`),
);

const logger = createLogger({
  format: formatOptions,
  transports: [
    new transports.Console({
      handleExceptions: true,
    }),
    new transports.File({
      filename: `./${logsDirectory}/${filename}.log`,
    }),
    new transports.DailyRotateFile({
      level: 'info',
      filename: `${directory}/${filename}-%DATE%.log`,
      datePattern: 'YYYY-MM-DD',
      json: true,
      maxFiles: '7d',
      maxSize: '10m',
      zippedArchive: true
    }),
  ],
  exitOnError: false
});

export default logger
