import dotenv from 'dotenv';

dotenv.config();

const environment = process.env.NODE_ENV;

const logsDirectory = process.env.LOGS_DIRECTORY;

const port = process.env.PORT;

const database = {
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  port: parseInt(process.env.POSTGRES_PORT || '5428'),
  name: process.env.POSTGRES_NAME,
  password: process.env.POSTGRES_PASSWORD
};

export {
  environment,
  logsDirectory,
  port,
  database
};
