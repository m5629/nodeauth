import {Pool} from "pg";

import connection from "./connection";
import logger from "../helpers/logger";

const pool = new Pool(connection);

pool.connect((err, client, release) => {
  if (err) {
    return logger.error('Error acquiring client', err.stack)
  }

  client.query('SELECT NOW()', (err, result) => {
    release()
    if (err) {
      return logger.error('Error executing query', err.stack)
    }
    logger.info(result.rows)
  })
})

export default pool
